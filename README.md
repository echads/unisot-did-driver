![DIF Logo](https://raw.githubusercontent.com/decentralized-identity/universal-resolver/master/docs/logo-dif.png)

# Universal Resolver Driver: did:unisot

This is a [Universal Resolver](https://github.com/decentralized-identity/universal-resolver/) driver for **did:unisot** identifiers.

## Specifications

- [W3C Decentralized Identifiers](https://w3c.github.io/did-core/)
- [UNISOT DID Method Specification](https://gitlab.com/unisot/documentation/unisot-did-method)

## Example DIDs

```
did:unisot:testnet:n1aAmTXAg4o44Z9k8YCQncEY91r3TV7WU4
```

## Build and Run (Docker)

```
docker build -f ./docker/Dockerfile . -t unisot/unisot-did-driver
docker run -p 8080:8080 unisot/unisot-did-driver
curl -X GET http://localhost:8080/1.0/identifiers/did:unisot:testnet:n1aAmTXAg4o44Z9k8YCQncEY91r3TV7WU4
```

## Build and Run

npm start

http://localhost:8080/1.0/identifiers/did:unisot:testnet:n1aAmTXAg4o44Z9k8YCQncEY91r3TV7WU4